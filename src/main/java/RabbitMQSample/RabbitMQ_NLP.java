package RabbitMQSample;

import com.questit.nlpp.data.QIText;
import com.questit.nlpp.data.serializer.QITextSerializer;
import com.questit.nlpp.resources.r0_ambient.QIAmbient;
import com.questit.nluplatform.client.NLUPlatformWebClient;

public class RabbitMQ_NLP extends RabbitMQ_AbstractConsumer {

	private static String NLP_INPUT_QUEUE = "toNLP";
	private static String NLP_OUTPUT_QUEUE = "toNLU";

	public static void main(String[] args) throws Exception {

		String name = "NLU";

		if (args.length > 0 && args[0] != null) {
			name = args[0].trim();
		}

		RabbitMQ_NLP myNLP = new RabbitMQ_NLP();

		myNLP.setServiceName(name);
		myNLP.setINPUT_TASK_QUEUE_NAME(new String[] { NLP_INPUT_QUEUE });
		myNLP.setOUTPUT_TASK_QUEUE_NAME(new String[] { NLP_OUTPUT_QUEUE });

		myNLP.onInit();

	}

	public byte[] doWork(String strMessage, String messageID) {

		byte[] ret = null;

		try {

			System.out.println("");

			QIText qiText = runNLP(strMessage);

			System.gc();

			ret = QITextSerializer.toJSONZippedBase64(qiText, "UTF-8").getBytes();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

		return ret;
	}

	private static QIText runNLP(String text) throws Exception {

		QIText qit = null;

		try {

			String base_url = "http://localhost:8080/NLUPlatformWeb/";

			final QIAmbient ambient = null;
			final String languageCode = "ita";

			System.out.println("*****************************************************************************");
			System.out.println("\t ");
			System.out.println("\t ... invocazione NLP:");
			System.out.println("\t \t NLP: " + base_url);
			System.out.println("\t ");
			System.out.println("\t... inizio elaborazione NLP ...");
			final NLUPlatformWebClient NLUClient = new NLUPlatformWebClient(base_url);
			System.out.println("\t ...");
			System.out.println("\t ...");

			qit = NLUClient.runNLPP(text, ambient, languageCode, true);
			System.out.println("\t ... processo NLP completato.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			qit = null;
		}

		return qit;

	}

}
