package RabbitMQSample;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.questit.nlpp.data.QIText;
import com.questit.nlpp.data.serializer.QITextDeserializer;
import com.questit.nlpp.data.serializer.QITextSerializer;
import com.questit.nlpp.resources.r0_ambient.QIAmbient;
import com.questit.nluplatform.client.NLUPlatformWebClient;
import com.questit.nluplatform.data.NLUAnnotation;
import com.questit.nluplatform.data.NLUQIText;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;

public abstract class RabbitMQ_AbstractConsumer {

	private static String[] INPUT_TASK_QUEUE_NAME;
	private static String[] OUTPUT_TASK_QUEUE_NAME;
	private static String serviceName = "";

	private static final String RABBIT_LOG_START = " [x] ";
	private static String QUEUE_HOST = "localhost";

	static boolean stop = false;

	private static Channel[] channelInput;
	private static Channel[] channelOutput;

	private static boolean isServiceFree = false;
	private static int maxMessage = 1;

	public static String[] getINPUT_TASK_QUEUE_NAME() {
		return INPUT_TASK_QUEUE_NAME;
	}

	public static void setINPUT_TASK_QUEUE_NAME(String[] iNPUT_TASK_QUEUE_NAME) {
		INPUT_TASK_QUEUE_NAME = iNPUT_TASK_QUEUE_NAME;
	}

	public static String[] getOUTPUT_TASK_QUEUE_NAME() {
		return OUTPUT_TASK_QUEUE_NAME;
	}

	public static void setOUTPUT_TASK_QUEUE_NAME(String[] oUTPUT_TASK_QUEUE_NAME) {
		OUTPUT_TASK_QUEUE_NAME = oUTPUT_TASK_QUEUE_NAME;
	}

	public static String getQUEUE_HOST() {
		return QUEUE_HOST;
	}

	public static String getServiceName() {
		return serviceName;
	}

	public static void setServiceName(String serviceName) {
		RabbitMQ_AbstractConsumer.serviceName = serviceName;
	}

	public static Channel[] getChannelInput() {
		return channelInput;
	}

	public static Channel[] getChannelOutput() {
		return channelOutput;
	}

	public static boolean isServiceFree() {
		return isServiceFree;
	}

	public static void setServiceFree(boolean isServiceFree) {
		RabbitMQ_AbstractConsumer.isServiceFree = isServiceFree;
	}

	public static int getMaxMessage() {
		return maxMessage;
	}

	public void onInit() {

		try {

			readQueue();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void readQueue() {

		System.out.println("SERVICE: " + serviceName);
		System.out.println("QUEUE_HOST: " + QUEUE_HOST);
		System.out.println("INPUT_QUEUE: " + INPUT_TASK_QUEUE_NAME[0]);
		if (OUTPUT_TASK_QUEUE_NAME != null) {
			System.out.println("OUTPUT_QUEUE: " + OUTPUT_TASK_QUEUE_NAME[0]);
		}

		Connection connectionInput = null;

		try {

			channelInput = new Channel[INPUT_TASK_QUEUE_NAME.length];

			if (OUTPUT_TASK_QUEUE_NAME != null && OUTPUT_TASK_QUEUE_NAME.length > 0) {

				channelOutput = new Channel[OUTPUT_TASK_QUEUE_NAME.length];
			}

			final ConnectionFactory factoryInput = new ConnectionFactory();
			factoryInput.setHost(QUEUE_HOST);
			connectionInput = factoryInput.newConnection();

			for (int i = 0; i < channelInput.length; i++) {
				final String queueName = INPUT_TASK_QUEUE_NAME[i];

				channelInput[i] = connectionInput.createChannel();

				channelInput[i].queueDeclare(queueName, true, false, false, null);

				System.out.println(" [*] " + serviceName + " - Waiting for messages on queue " + queueName
						+ " To exit press CTRL+C");
				channelInput[i].basicQos(maxMessage);
			}

			isServiceFree = true;

			final DeliverCallback deliverCallback = new DeliverCallback() {

				public void handle(final String consumerTag, final Delivery delivery) throws IOException {
					int numQueue = -1;
					try {

						final String currQueue = delivery.getEnvelope().getRoutingKey();

						numQueue = getInputChannelNumberByName(currQueue);

						while (!isServiceFree) {
							Thread.sleep(1000);
						}

						if (isServiceFree) {
							isServiceFree = false;
							try {
								String myMessage = new String(delivery.getBody(), "UTF-8");

								System.out.println(RabbitMQ_AbstractConsumer.RABBIT_LOG_START + serviceName
										+ " - Received '" + delivery.getProperties().getMessageId() + "' on queue "
										+ currQueue + " ... ");

								System.out.println(RabbitMQ_AbstractConsumer.RABBIT_LOG_START + serviceName
										+ " - ... processing ... ");

								byte[] msgOutput = doWork(myMessage, delivery.getProperties().getMessageId());

								System.out.println(RabbitMQ_AbstractConsumer.RABBIT_LOG_START + serviceName
										+ " - Processed '" + delivery.getProperties().getMessageId() + "' on queue "
										+ currQueue + "... ");

								if (OUTPUT_TASK_QUEUE_NAME != null && OUTPUT_TASK_QUEUE_NAME.length > 0
										&& msgOutput != null) {
									System.out.println(RabbitMQ_AbstractConsumer.RABBIT_LOG_START + serviceName
											+ " - ... sending on output queue " + OUTPUT_TASK_QUEUE_NAME[0]);
									writeToOutput(msgOutput);
								}

							} catch (final Exception e) {

							} finally {
								isServiceFree = true;
							}
						}

					} catch (final Exception e) {

						e.printStackTrace();
					} finally {

						if (numQueue > -1) {
							channelInput[numQueue].basicAck(delivery.getEnvelope().getDeliveryTag(), false);
							System.out.println(" [*] ... ");
							System.out.println(" [*] " + serviceName + " - Waiting for messages on queue "
									+ INPUT_TASK_QUEUE_NAME[numQueue] + ". To exit press CTRL+C");
						}
					}
				}
			};

			for (int i = 0; i < channelInput.length; i++) {
				channelInput[i].basicConsume(INPUT_TASK_QUEUE_NAME[i], false, deliverCallback, consumerTag -> {
				});
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {

		}
	}

	public abstract byte[] doWork(String strMessage, String messageID);

	private static void writeToOutput(final byte[] messageByte) throws Exception {

		final ConnectionFactory factoryOutput = new ConnectionFactory();
		factoryOutput.setHost(QUEUE_HOST);

		final Connection connectionOutput = factoryOutput.newConnection();

		for (int c = 0; c < channelOutput.length; c++) {
			final String queueName = OUTPUT_TASK_QUEUE_NAME[c];

			channelOutput[c] = connectionOutput.createChannel();
			channelOutput[c].queueDeclare(queueName, true, false, false, null);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

			channelOutput[c].basicPublish("", queueName, new AMQP.BasicProperties.Builder().contentType("text/plain")
					.deliveryMode(2).priority(1).messageId(timeStamp).build(), messageByte);
			System.out.println(RabbitMQ_AbstractConsumer.RABBIT_LOG_START + serviceName
					+ " - ... message sent to queue " + queueName);
		}
	}

	private static int getInputChannelNumberByName(final String name) {
		int ret = -1;

		for (int i = 0; i < INPUT_TASK_QUEUE_NAME.length; i++) {
			if (INPUT_TASK_QUEUE_NAME[i].trim().equals(name.trim())) {
				ret = i;
				break;
			}
		}

		return ret;
	}

}
