package RabbitMQSample;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.questit.nluplatform.data.NLUAnnotation;
import com.questit.nluplatform.data.NLUQIText;

public class RabbitMQ_TEXT extends RabbitMQ_AbstractConsumer {

	private static String TXT_INPUT_QUEUE = "toTEXT";

	static boolean stop = false;

	public static void main(String[] args) throws Exception {

		String name = "TXT";

		if (args.length > 0 && args[0] != null) {
			name = args[0].trim();
		}

		RabbitMQ_TEXT myTXT = new RabbitMQ_TEXT();

		myTXT.setServiceName(name);
		myTXT.setINPUT_TASK_QUEUE_NAME(new String[] { TXT_INPUT_QUEUE });
		myTXT.setOUTPUT_TASK_QUEUE_NAME(null);

		myTXT.onInit();

	}

	public byte[] doWork(String strMessage, String messageID) {

		byte[] ret = null;

		try {

			NLUQIText nluQI = NLUQIText.fromJSON(strMessage);

			try {
				String filePath = "D:\\RABBIT\\out\\" + messageID + ".out";
				writeToFile(filePath, nluQI);
			} catch (Exception e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

		return ret;
	}

	private static boolean writeToFile(String filePath, NLUQIText nluQI) {
		boolean ret = false;
		try {
			File outFile = new File(filePath);
			if (outFile.createNewFile()) {
				System.out.println("File created: " + outFile.getName());
			} else {
				System.out.println("File already exists.");
			}

			BufferedWriter osw = new BufferedWriter(new FileWriter(outFile, true));

			osw.append(nluQI.getQIText().getText() + "\n");
			osw.append("\n");
			osw.append("\n");

			ArrayList<NLUAnnotation> annotations = nluQI.getAllAnnotations();

			for (int a = 0; a < annotations.size(); a++) {

				NLUAnnotation curr = annotations.get(a);

				osw.append(curr.getAnnotatatedText() + "\n");
			}
			osw.close();

			ret = true;
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return ret;
	}

}
