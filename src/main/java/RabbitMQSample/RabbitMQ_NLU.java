package RabbitMQSample;

import java.util.ArrayList;

import com.questit.nlpp.data.QIText;
import com.questit.nlpp.data.serializer.QITextDeserializer;
import com.questit.nlpp.resources.r0_ambient.QIAmbient;
import com.questit.nluplatform.client.NLUPlatformWebClient;
import com.questit.nluplatform.data.NLUQIText;

public class RabbitMQ_NLU extends RabbitMQ_AbstractConsumer {

	private static String NLU_INPUT_QUEUE = "toNLU";
	private static String NLU_OUTPUT_QUEUE = "toTEXT";

	static boolean stop = false;

	public static void main(String[] args) throws Exception {

		String name = "NLU";

		if (args.length > 0 && args[0] != null) {
			name = args[0].trim();
		}

		RabbitMQ_NLU myNLU = new RabbitMQ_NLU();

		myNLU.setServiceName(name);
		myNLU.setINPUT_TASK_QUEUE_NAME(new String[] { NLU_INPUT_QUEUE });
		myNLU.setOUTPUT_TASK_QUEUE_NAME(new String[] { NLU_OUTPUT_QUEUE });

		myNLU.onInit();

	}

	public byte[] doWork(String strMessage, String messageID) {

		byte[] ret = null;

		try {

			System.out.println("");

			NLUQIText nluQI = runNLU(strMessage);

			System.gc();

			ret = nluQI.toJSON(false).getBytes();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

		return ret;
	}

	private static NLUQIText runNLU(String strQIText) throws Exception {

		NLUQIText qit = null;

		try {
			String user_id = "gianni";
			String model_id = "esempio_frutta";
			String annotator = "mainannotator";

			String base_url = "http://localhost:8080/NLUPlatformWeb/";

			QIText qitext = QITextDeserializer.fromJSONZippedBase64(strQIText, "UTF-8");
			final QIAmbient ambient = null;
			final String languageCode = "ita";

			System.out.println("*****************************************************************************");
			System.out.println("\t ");
			System.out.println("\t ... invocazione NLU:");
			System.out.println("\t \t NLU: " + base_url);
			System.out.println("\t \t user_id: " + user_id);
			System.out.println("\t \t model_id: " + model_id);
			System.out.println("\t ");
			System.out.println("\t... inizio elabaorazione NLU ...");
			final NLUPlatformWebClient NLUClient = new NLUPlatformWebClient(base_url);
			System.out.println("\t ...");

			ArrayList<String> models_id = new ArrayList<String>();
			models_id.add(model_id);
			System.out.println("\t ...");

			qit = NLUClient.runModelSelectingAnnotatorV2("vorrei comprare una banana", qitext, ambient, user_id,
					models_id, languageCode, annotator);
			System.out.println("\t ... processo NLU completato.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			qit = null;
		}

		return qit;

	}
}
